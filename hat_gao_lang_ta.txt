        HẠT GẠO LÀNG TA - Trần Đăng Khoa
 Hạt gạo làng ta là bài thơ của nhà thơ Trần Đăng Khoa sáng tác năm 1969,
 được in trong tập thơ Góc Sân Và Khoàng Trời 1968

        Hạt gạo làng ta
        Có vị phù sa
        Của sông Kinh Thầy
        Có hương sen thơm
        Trong hồ nước đầy
        Có lời mẹ hát
        Ngọt bùi đắng cay

        Hạt gạo làng ta
        Có bão tháng bảy
        Có mưa tháng ba
        giọt mồ hôi sa
        Những trưa tháng sáu
        Nước như ai nấu
        Chết cả cá cờ
        Cua ngoi lên bờ
        Mẹ em xuống cấy

        Hạt gạo làng ta
        những năm bom đạn
        Trút lên mái nhà
        Những năm cây súng
        Theo người đi xa
        Những năm băng đạn
        vàng như lúa đồng
        bát cơm mùa gặt
        thơm hào giao thông

        Trưa nào chống hạn
        Tát mẻ miệng gầu
        Hạt gạo làng ta
        Có công các bạn

        Hạt gạo làng ta
        Có công các bạn

        Hạt gạo làng ta
        Gửi ra tiền tuyến
        Gửi về phương xa
        Em vui em hát
        Hạt vàng làng ta.
